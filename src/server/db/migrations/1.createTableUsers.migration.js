const {DataTypes} = require('sequelize');


function up(queryInterface) {
    return queryInterface.createTable(
        'User',
        {
            username: {
                field: 'Username',
                type: DataTypes.STRING(100),
                allowNull: false,
                primaryKey: true,
                validate: {
                    isEmail: true,
                    notEmpty: true,
                },
            },
            password: {
                field: 'Password',
                type: DataTypes.STRING(100),
                allowNull: false,
                validate: {
                    notEmpty: true,
                },
            },
            createdOn: {
                field: 'CreatedOn',
                type: DataTypes.DATE,
                allowNull: false,
            },
            changedOn: {
                field: 'ChangedOn',
                type: DataTypes.DATE,
                allowNull: false,
            },
        },
    );
}


function down(queryInterface) {
    return queryInterface.dropTable('user');
}


module.exports = {up, down};
