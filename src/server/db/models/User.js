const {DataTypes} = require('sequelize');


function init(instance) {
    return instance.define('User', {
        username: {
            field: 'Username',
            type: DataTypes.STRING(100),
            allowNull: false,
            primaryKey: true,
            validate: {
                isEmail: true,
                notEmpty: true,
            },
        },
        password: {
            field: 'Password',
            type: DataTypes.STRING(100),
            allowNull: false,
            validate: {
                notEmpty: true,
            },
        },
        createdOn: {
            field: 'CreatedOn',
            type: DataTypes.DATE,
            allowNull: false,
        },
        changedOn: {
            field: 'ChangedOn',
            type: DataTypes.DATE,
            allowNull: false,
        },
    }, {
        timestamps: true,
        freezeTableName: true,
        tableName: 'User',
        createdAt: 'createdOn',
        updatedAt: 'changedOn',
    });
}


module.exports = init;
