const session = require('express-session');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const helmet = require('helmet');

const fs = require('fs');
const path = require('path');


function appMiddleware(app) {
    app.use(bodyParser.json());
    app.use(cookieParser());
    app.use(helmet());
}


function responseHeaders(app) {
    app.use(function (req, res, next) {
        res.header('Access-Control-Allow-Origin', '*');
        res.header(
            'Access-Control-Allow-Headers',
            'Origin, X-Requested-With, Content-Type, Accept, Cache-control, Expires, Pragma, ETag',
        );
        res.header('Access-Control-Request-Method', 'GET, POST, PUT, DELETE, OPTIONS');
        res.header('Cache-Control', 'max-age=0, must-revalidate');
        res.header('Expires', 0);

        next();
    });
}


function appSession(app) {
    app.use(session({
        secret: 'fintess secret',
        resave: true,
        saveUninitialized: true,
        name: 'fitness.connect.sid',
    }));
}


function routes(app) {
    const files = fs.readdirSync(path.resolve('src/server/route'));
    files.forEach((file) => {
        if (file.indexOf('Route.js') >= 0) {
            require(`./../route/${file}`).index(app, require(`./../controller/${file.replace('Route', 'Controller')}`));
        }
    });
}


function runServer(app) {
    const exitHandler = (options = {exit: false, cleanup: false}) => {
        return (code) => {
            if (code) {
                console.log(`about to exit with code '${code}'`);
            }
            if (options.exit) {
                process.exit();
            }
        };
    };
    // do something when app is closing
    process.on('exit', exitHandler({cleanup: true}));
    // catches ctrl+c event
    process.on('SIGINT', exitHandler({exit: true}));
    // catches uncaught exceptions
    process.on('uncaughtException', exitHandler({exit: true}));

    let port = (process.env.PORT || 3000);
    app.listen(port, () => {
        console.info(`The server is running at ${process.env.HOST || 'localhost'}:${port}`);
    });
}


module.exports = {
    appMiddleware: appMiddleware,
    responseHeaders: responseHeaders,
    appSession: appSession,
    routes: routes,
    runServer: runServer,
};
